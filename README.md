## HASCIICAM 

## Developer maintainer website :: Dynabolic :: 
- https://www.dyne.org/software/hasciicam/

- https://github.com/jaromil/hasciicam

### Live session 400x500
- hasciicam -s 400 500   

#### examples html fonts
- hasciicam -a bitsream  -S 2 -m html -o hasciicam.html
- hasciicam -a verdana -S 2 -m html -o hasciicam.html 
- hasciicam -a arial  -S 3 -m html -o hasciicam.html
- hasciicam -a noto -S 2 -m html -o hasciicam.html 
- hasciicam -a nimbus -S 3 -m html -o hasciicam.html 
- hasciicam -a ubuntu -S 2 -m html -o hasciicam.html 

![E-snap](/uploads/e5f913fd011e53ff247c760614b0d176/E-snap.png)

#### ARCH dependencies >>>> logout / login
- xorg-font-util

- xorg-fonts-100dpi

- xorg-fonts-75dpi

- xorg-fonts-alias

- xorg-fonts-encodings

- xorg-fonts-misc

- xorg-fonts-type1

- xorg-mkfontscale

- xorg-xlsfonts

---

### When hasciicam has launch problems. I am using bunsenlabs openbox 32 bit.

> `ERROR : Segment Fault.`

- it could be cursor theme.


##### STEP1 ::

`$ sudo update-alternatives --config x-cursor-theme`

_I chose the default theme._

##### STEP2 ::

Within a cursors folder in __/usr/share/icons/<cursorName>__ there is an __index.theme__ file.

If that index.theme file contains an __"Inherits"__ line such as: 

-----------

[Icon Theme]  

> Name = Comix Cursor original Blue Large Bold

> Comment = The original Comix Cursors - Blue Large Bold

> Inherits = ComixCursors-Blue-Large

> Example = default


- Remove the line : _Inherits = ComixCursors-Blue-Large_

 [Source ::](https://forums.linuxmint.com/viewtopic.php?t=225214)



---

After these changes Hasciicam has still issues.

I figured when i launch hasciicam with Alt-F2 <hasciicam> , hasciicam launches. But does not launch from the ob-menu or terminal.

---

###### new edit

when i do :

- Alt+F2 sudo hasciicam. 
- Hasciicam runs as root. I close hasciicam.
- Again Alt+F2 and now run hasciicam, as user. 

Now hasciicam launches. After closing, I need to do the same steps as before to lauch hasciicam again as user.

---

